{ stdenv, fetchurl, unzip, patchelf, lib }:

let
  libpath = lib.makeLibraryPath [
    stdenv.cc.cc
  ];
in

stdenv.mkDerivation {
  name         = "dat-13.10.0-linux-x64";
  src          = fetchurl {
    url    = "https://github.com/datproject/dat/releases/download/v13.10.0/dat-13.10.0-linux-x64.zip";
    sha256 = "00qspa3j300jq9l5kg4v01b923800nysjrcl60jipd47260r8r5v";
  };
  dontStrip    = true;
  buildInputs  = [ unzip patchelf ];
  installPhase = ''
    mkdir -p $out/bin
    cp dat $out/bin
    patchelf \
      --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
      --set-rpath "$out/bin:${libpath}" \
      $out/bin/dat
  '';
}
